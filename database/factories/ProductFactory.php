<?php

namespace Database\Factories;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'supplier_id' => Supplier::factory(),
            'code' => str(fake()->bothify('???###'))->upper(),
            'name' => str(fake()->words(3, true))->headline(),
            'description' => fake()->sentence(),
            'price' => (integer) fake()->numerify('###00')
        ];
    }
}
