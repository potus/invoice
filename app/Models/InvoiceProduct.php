<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Models\InvoiceProduct
 *
 * @property int $invoice_id
 * @property int $product_id
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read int $sub_total_price
 * @property-read \App\Models\Invoice $invoice
 * @property-read \App\Models\Product $product
 * @method static \Database\Factories\InvoiceProductFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\InvoiceProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class InvoiceProduct extends Pivot
{
    use HasFactory;

    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
