<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property int $customer_id
 * @property string $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Customer $customer
 * @property-read mixed $sub_total
 * @property-read mixed $sub_total_formatted
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Product> $products
 * @property-read int|null $products_count
 * @method static \Database\Factories\InvoiceFactory factory($count = null, $state = [])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Invoice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer_id'
    ];

    protected $appends = [
        'sub_total',
        'sub_total_formatted',
        'tax_formatted',
        'total_formatted'
    ];

    /**
     * The "booted" method of the model.
     */
    protected static function booted(): void
    {
        static::creating(function (Invoice $invoice) {
            $invoice->code = str(fake()->bothify('???###'))->upper();
        });
    }

    /**
     * Get the customer that owns the invoice.
     */
    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, InvoiceProduct::class)->withPivot(['quantity']);
    }

    public function getSubTotalAttribute()
    {
        return $this->products->sum('ammount');
    }

    public function getSubTotalFormattedAttribute()
    {
        return $this->sub_total ? "Rp. " . number_format($this->sub_total, 2, ',', '.') : "Rp. 0,00";
    }

    public function getTaxAttribute()
    {
        return $this->sub_total * (11 / 100);
    }

    public function getTaxFormattedAttribute()
    {
        return $this->tax ? "Rp. " . number_format($this->tax, 2, ',', '.') : "Rp. 0,00";
    }

    public function getTotalAttribute()
    {
        return $this->sub_total + $this->tax;
    }

    public function getTotalFormattedAttribute()
    {
        return $this->total ? "Rp. " . number_format($this->total, 2, ',', '.') : "Rp. 0,00";
    }
}
