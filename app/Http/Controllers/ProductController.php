<?php

namespace App\Http\Controllers;

use Inertia\{Inertia, Response};
use App\Models\{Product, Supplier};
use Illuminate\Http\{RedirectResponse, Request};

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): Response
    {
        return Inertia::render('Product/Index', [
            'products' => Product::with(['supplier'])->withCount(['invoices'])->latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        return Inertia::render('Product/Form', [
            'suppliers' => Supplier::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'string|max:255',
            'description' => 'string|max:255',
            'price' => 'integer',
            'supplier.id' => 'int|exists:App\Models\Supplier,id'
        ]);

        Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'supplier_id' => $request->supplier['id']
        ]);

        return to_route('products.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product): RedirectResponse
    {
        return to_route('products.edit', $product);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product): Response
    {
        return Inertia::render('Product/Form', [
            'suppliers' => Supplier::all(),
            'product' => $product->loadMissing(['supplier'])
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product): RedirectResponse
    {
        $request->validate([
            'name' => 'string|max:255',
            'description' => 'string|max:255',
            'price' => 'integer',
            'supplier.id' => 'int|exists:App\Models\Supplier,id'
        ]);

        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'supplier_id' => $request->supplier['id']
        ]);

        return to_route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product): RedirectResponse
    {
        // try {
            if ($product->delete()) {
                return to_route('products.index');
            }
        // } catch (\Throwable $th) {
            return back();
        // }
    }
}
