<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\Customer;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return Inertia::render('Invoice/Index', [
            'invoices' => Invoice::with(['customer', 'products'])->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::render('Invoice/Form', [
            'customers' => Customer::all(),
            'products' => Product::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer.id' => 'int|exists:App\Models\Customer,id'
        ]);

        $invoice = Invoice::create([
            'customer_id' => $request->customer['id']
        ]);

        return to_route('invoices.show', $invoice);
    }

    /**
     * Display the specified resource.
     */
    public function show(Invoice $invoice)
    {
        return Inertia::render('Invoice/Show', [
            'invoice' => $invoice->loadMissing(['customer', 'products']),
            'products' => Product::whereNotIn('id', $invoice->products->pluck('id'))
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Invoice $invoice)
    {
        return Inertia::render('Invoice/Form', [
            'invoice' => fn () => $invoice->loadMissing(['customer', 'products']),
            'customers' => Customer::all(),
            'products' => Product::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Invoice $invoice)
    {
        $invoice->products()->sync([]);

        if ($invoice->delete()) {
            return back();
        }

        return back();
    }
}
