<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Invoice $invoice)
    {
        return Inertia::render('Invoice/AddProduct', [
            'invoice' => $invoice->loadMissing(['customer', 'products']),
            'products' => Product::whereNotIn('id', $invoice->products->pluck('id'))->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Invoice $invoice)
    {
        $request->validate([
            'product.id' => 'int|exists:App\Models\Product,id',
            'quantity' => 'int|min:1'
        ]);

        $invoice->products()->syncWithoutDetaching([
            $request->product['id'] => ['quantity' => $request->get('quantity')]
        ]);

        return to_route('invoices.show', $invoice);
    }

    /**
     * Display the specified resource.
     */
    public function show(Invoice $invoice, Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Invoice $invoice, Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Invoice $invoice, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Invoice $invoice, Product $product)
    {
        //
    }
}
