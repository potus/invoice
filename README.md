## Run Project

Before start

1. change .env.example to .env
2. (only for windows os) delete "DB_DATABASE=/var/www/html/database/database.sqlite" in .env file

STEP 1 IN TERMINAL

1. composer install
2. npm install
3. npm run build
4. npm run dev

notes : step 4 is optional

STEP 2 OPEN IN NEW TERMINAL

For Non existing database

1. php artisan key:generate
2. php artisan migrate
3. php artisan serve

For Existing Database

1. php artisan key:generate
2. php artisan db:wipe
3. php artisan migrate
4. php artisan db:seed
5. php artisan serve
6. Open browser in type http://127.0.0.1:8080
7. login with email admin@example.com (password: password)